//
//  ShoppingListViewController.swift
//  ShoppingList
//
//  Created by Vrushali Kulkarni on 04/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

enum UITableViewActionMode {
    case edit
    case add
}

final class ShoppingListViewController: UITableViewController {

    private let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

    private var actionState: UITableViewActionMode = .add
    private var shoppingItems = [String]()
    private var fileURL: URL {
        return path.appendingPathComponent("ShoppingList.txt")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        readListFromFile()
    }
}

private extension ShoppingListViewController {

    func readListFromFile() {

        do {
            let contents = try String(contentsOf: fileURL)
            shoppingItems += contents.components(separatedBy: "\n")
        } catch {
            print(error)
        }
    }

    func writeListToFile() {

        let string = shoppingItems.joined(separator: "\n")
        do {
            try string.write(to: fileURL, atomically: false, encoding: .utf8)
        } catch {
            print(error)
        }
    }
}

private extension ShoppingListViewController {

    func configureUI() {

        title = "Shopping List"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Clear List",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(clearList))

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(promptForItem(at:)))

        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let add = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        toolbarItems = [spacer, add]
        navigationController?.isToolbarHidden = false
    }
}

private extension ShoppingListViewController {

    @objc func clearList() {
        shoppingItems.removeAll()
        tableView.reloadData()
    }

    @objc func promptForItem(at: Any) {

        let alertController = UIAlertController(title: "Enter Item", message: "", preferredStyle: .alert)
        alertController.addTextField()
        let action = UIAlertAction(title: "Submit", style: .default) { [weak self, weak alertController] _ in
            self?.validate(item: alertController?.textFields?[0].text, at: at)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }

    @objc func shareTapped() {

        let list = shoppingItems.joined(separator: "\n")
        let viewController = UIActivityViewController(activityItems: [list],
                                                      applicationActivities: [])
        viewController.popoverPresentationController?.barButtonItem = navigationController?.toolbarItems?.last
        present(viewController, animated: true, completion: nil)
    }
}

private extension ShoppingListViewController {

    func validate(item: String?, at indexPath: Any) {

        let errorTitle: String
        let errorMessage: String

        guard let item = item, !item.isEmpty else {
            errorTitle = "Empty Item!"
            errorMessage = "Cannot add empty item. Please enter item."
            showAlert(title: errorTitle, message: errorMessage)
            return
        }

        guard item.count > 2 else {
            errorTitle = "Item name small!"
            errorMessage = "Please add a descriptive name!"
            showAlert(title: errorTitle, message: errorMessage)
            return
        }

        let capitalFirstLetter = item.capitalizedFirstLetter()
        guard !shoppingItems.contains(capitalFirstLetter) else {

            errorTitle = "Already added!"
            errorMessage = "Item already added. Please add a new one."
            showAlert(title: errorTitle, message: errorMessage)
            return
        }

        switch actionState {
        case .add:
            self.addToList(item: capitalFirstLetter)
        case .edit:
            addUpdatedItem(item: capitalFirstLetter, at: indexPath)
        }
    }

    func showAlert(title: String, message: String) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }

    func addToList(item: String) {

        self.shoppingItems.insert(item, at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        writeListToFile()
    }

    func addUpdatedItem(item: String, at indexpath: Any) {

        guard let indexpath = indexpath as? IndexPath else { return }
        shoppingItems.remove(at: indexpath.row)
        tableView.deleteRows(at: [indexpath], with: .automatic)
        shoppingItems.insert(item, at: indexpath.row)
        tableView.insertRows(at: [indexpath], with: .automatic)
    }
}

extension ShoppingListViewController {

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return shoppingItems.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingItem", for: indexPath)
        cell.textLabel?.text = shoppingItems[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
            self?.shoppingItems.remove(at: indexPath.row)
            self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            self?.writeListToFile()
        }

        let editAction = UIContextualAction(style: .normal, title: "Edit") { [weak self] (_, _, _) in

            self?.actionState = .edit
            self?.promptForItem(at: indexPath)
        }

        deleteAction.backgroundColor = .red
        editAction.backgroundColor = .blue
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}
