//
//  CapitalizedString.swift
//  ShoppingList
//
//  Created by Vrushali Kulkarni on 04/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import Foundation

extension String {
    func capitalizedFirstLetter() -> String {

        return prefix(1).capitalized + dropFirst()
    }
}
